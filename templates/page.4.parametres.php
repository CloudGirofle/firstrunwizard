<?php
/**
 * @copyright Copyright (c) 2018 Julius Härtl <jus@bitgrid.net>
 *
 * @author Julius Härtl <jus@bitgrid.net>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * @var array $_
 * @var \OCP\IL10N $l
 * @var \OCP\Defaults $theme
 */
?>

<div class="page" data-title="Bienvenue !" data-subtitle=""
          style="
    height:100%;
    width:100%;
    background-image: url('<?php p(image_path('firstrunwizard', 'background-cloud.png')); ?>');
    font-size:1em;
    " >
	<div class="content content-values">
          <h2>Paramètres</h2>

    <div style="text-align:center">
    <img src="<?php p(image_path('firstrunwizard', 'parametres.png')); ?>" style="width:70%"/>
    </div>
          
        <p>Le <em>Nom complet</em> permet de personnaliser son compte auquel peut être ajouté en <em>Photo de profil</em> un avatar.</p>

	</div>
</div>


       
