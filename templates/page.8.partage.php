<?php
/**
 * @copyright Copyright (c) 2018 Julius Härtl <jus@bitgrid.net>
 *
 * @author Julius Härtl <jus@bitgrid.net>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * @var array $_
 * @var \OCP\IL10N $l
 * @var \OCP\Defaults $theme
 */
?>

<div class="page" data-title="Bienvenue !" data-subtitle=""
          style="
    height:100%;
    width:100%;
    background-image: url('<?php p(image_path('firstrunwizard', 'background-cloud.png')); ?>');
    font-size:1em;
    " >
	<div class="content content-values">

       <ul id="wizard-values">
            <li style="text-align: center; width:30%; margin:auto;">
          <h2>Partage de documents</h2>
          <p>Le partage de documents peut se faire : </p>

          <ul style="line-height: 10px;text-align: left;">
            <li>Avec un⋅e <em>utilisateur⋅ice</em></li>
            <li>Avec un <em>groupe</em></li>
            <li>Avec l'<em>adresse mail</em> d'une personne extérieure</li>
            <li>Par <em>lien</em></li>
            <li>Sur une autre <em>instance Nextcloud</em></li>
          </ul>
          <img src="<?php p(image_path('firstrunwizard', 'partagedoc.svg')); ?>" style="width:60%"/>
            </li>
            <li style="; width:45%; margin:auto;">

          <video width="80%" height="80%" controls autoplay>
          <source src="<?php p(image_path('firstrunwizard', 'videopartage.ogv')); ?>" type="video/ogg">
          <source src="<?php p(image_path('firstrunwizard', 'videopartage.webm')); ?>" type="video/webm">
          </video>
            </li>
        </ul>

</div>


       
