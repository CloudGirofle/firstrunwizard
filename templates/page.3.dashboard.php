<?php
/**
 * @copyright Copyright (c) 2018 Julius Härtl <jus@bitgrid.net>
 *
 * @author Julius Härtl <jus@bitgrid.net>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * @var array $_
 * @var \OCP\IL10N $l
 * @var \OCP\Defaults $theme
 */
?>

<div class="page" data-title="Dashboard !" data-subtitle=""
          style="
    height:100%;
    width:100%;
    background-image: url('<?php p(image_path('firstrunwizard', 'background-cloud.png')); ?>');
    font-size:1em;
    " >
	<div class="content content-values">
          <h2>Fonctionnement du tableau de bord (Dashboard)</h2>

    <div style="text-align:center">
    <img src="<?php p(image_path('firstrunwizard', 'dashboard.svg')); ?>" style="width:70%"/>
    </div>
    
    <ul id="wizard-values" style="line-height: 8px;">
    <li style="width:35%; margin:auto;">
    <ul><li>1. Retour au tableau de bord</li>
    <li>2. Fichiers</li>
    <li>3. Photos</li>
    <li>4. Historique des activités</li>
    <li>5. Agenda</li>
    </ul></li>
    
    <li style="width:35%; margin:auto;">
      <ul>
        <li>6. Outil de recherche</li>
        <li>7. Notifications</li>
        <li>8. Recherche de contacts</li>
        <li>9. Paramètres du compte/déconnexion</li>
      </ul>
    </li>
    </ul>
	</div>
</div>


       
