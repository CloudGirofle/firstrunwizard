<?php
/**
 * @copyright Copyright (c) 2018 Julius Härtl <jus@bitgrid.net>
 *
 * @author Julius Härtl <jus@bitgrid.net>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * @var array $_
 * @var \OCP\IL10N $l
 * @var \OCP\Defaults $theme
 */
?>

<div class="page" data-title="Bienvenue !" data-subtitle=""
          style="
    height:100%;
    width:100%;
    background-image: url('<?php p(image_path('firstrunwizard', 'background-cloud.png')); ?>');
    font-size:1.3em;
    " >
	<div class="content content-values">
       <p>Vous utilisez une application de <b>stockage</b> de documents, de <b>partage</b> et de <b>collaboration</b> mise à disposition par le Cloud Girofle.</p>
       <p>Cette application utilise les logiciels Nextcloud et OnlyOffice.</p>

       <ul id="wizard-values">
            <li style="text-align: center; width:35%; margin:auto;">
              <img src="<?php p(image_path('firstrunwizard', 'icon-nextcloud.png')); ?>" style="width:70%"/>
            </li>
            <li style="text-align: center; width:35%; margin:auto;">
              <img src="<?php p(image_path('firstrunwizard', 'icon-onlyoffice.png')); ?>" style="width:70%"/>
            </li>
        </ul>

          
        <p>Ce tutoriel présente les principales fonctionnalités de ces logiciels.</p>

	</div>
</div>


       
