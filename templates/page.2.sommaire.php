<?php
/**
 * @copyright Copyright (c) 2018 Julius Härtl <jus@bitgrid.net>
 *
 * @author Julius Härtl <jus@bitgrid.net>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * @var array $_
 * @var \OCP\IL10N $l
 * @var \OCP\Defaults $theme
 */
?>

<div class="page" data-title="Sommaire !" data-subtitle=""
          style="
    height:100%;
    width:100%;
    background-image: url('<?php p(image_path('firstrunwizard', 'background-cloud.png')); ?>');
    font-size:1.3em;
    " >
	<div class="content content-values">
       <h2>Sommaire</h2>
       <ul>
          <li>Interface générale - 
              <span style="padding-right:3px; padding-top: 3px; display:inline-block;">
              <img class="manImg" src="<?php p(image_path('firstrunwizard', 'icon-nextcloud.png')); ?>" style="height:1.5em;"></img>
              </span>
          </li>
          <li>Partage de documents -
              <span style="padding-right:3px; padding-top: 3px; display:inline-block;">
              <img class="manImg" src="<?php p(image_path('firstrunwizard', 'share.png')); ?>" style="height:1.5em;"></img>
              </span>          
          </li>
          <li>Édition collaborative - 
              <span style="padding-right:3px; padding-top: 3px; display:inline-block;">
              <img class="manImg" src="<?php p(image_path('firstrunwizard', 'icon-onlyoffice.png')); ?>" style="height:1.5em;"></img>
              </span>          
          </li>
          <li>Synchronisation des documents -
              <span style="padding-right:3px; padding-top: 3px; display:inline-block;">
              <img class="manImg" src="<?php p(image_path('firstrunwizard', 'sync.png')); ?>" style="height:1.5em;"></img>
              </span>          
          </li>
          <li>Calendriers -
              <span style="padding-right:3px; padding-top: 3px; display:inline-block;">
              <img class="manImg" src="<?php p(image_path('firstrunwizard', 'calendar.png')); ?>" style="height:1.5em;"></img>
              </span>          
          </li>
       </ul>
	</div>
</div>


       
