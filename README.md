# Cloud Girofle

Ceci est un clone du repo officiel, qui contient les modifications liées au CloudGirofle.

## Installation

```
sudo -u www-data php occ app:disable firstrunwizard
cd apps;
mv firstrunwizard firstrunwizard.bak;
git clone https://framagit.org/CloudGirofle/firstrunwizard-girofle.git firstrunwizard; 
chown -R girofle:www-data firstrunwizard/
cd ..
sudo -u www-data php occ app:enable firstrunwizard
```

# Mettre à jour le repo

Pour mettre à jour le repo, il faut que vous ayez configuré le repo officiel (que l'on va nommer `upstream`) dans vos remotes (à ne faire que la première fois)

```
git remote add upstream git@github.com:nextcloud/firstrunwizard.git
```

Ensuite vous pouvez puller les dernieres mise à jour. Ex pour la branche `stable27`

```
git pull upstream stable27
```

Puis rebase la branche `girofle` sur la branche que vous venez de pull

```
git checkout girofle
git rebase stable27
```

Puis créer un nouveau tag, et mettre à jour la branche `girofle` sur notre repo

```
git tag -a v27
git push origin girofle -f
```

## Modifications

On a:
- ajouté des templates dans le dosser `templates/`
- modifié le fichier `lib/Controller/WizardController.php` pour prendre en compte nos nouveaux templates

# 🔮 First run wizard

A first run wizard that explains the usage of Nextcloud to new users

![](https://user-images.githubusercontent.com/3404133/51537050-bcc73e00-1e4d-11e9-8de0-29e6951c2b29.png)


## Development setup

Make sure you have `node`, `npm` and `make` installed on your system.

1. ☁ Clone the app into the `apps` folder of your Nextcloud: `git clone https://github.com/nextcloud/firstrunwizard.git`
2. 👩‍💻 Run `make dev-setup` to install the dependencies
3. 🏗 To build the Javascript after you have made changes, run `make build-js`
4. ✅ Enable the app through the app management of your Nextcloud
5. 🎉 Partytime! Help fix [some issues](https://github.com/nextcloud/firstrunwizard/issues) and [review pull requests](https://github.com/nextcloud/firstrunwizard/pulls) 👍
